
# Projet Consommations de service WEB

Ce depot concerne le projet à rendre pour la SAE.

## Informations sur le projet
Dans le fichier racine __/src/main/java/com/api/hearttrackpicture__:

Notre projet est réalisé en SpringBoot avec:
- Les models dans le dossier __/models__
- Les controllers utilisés dans __/controllers__
- Les repositoriese dans __/repositories__
- Le stub dans __/stub__
## Installation

Il suffit de réaliser ces commandes dans l'ordre:

```bash
  git clone <notre-projet>
  cd <notre-projet>
  mvn clean install
  mvn package
  mvn spring-boot:run
```
Ensuite, il suffit d'ouvrir votre navigateur avec le lien localhost obtenu avec le terminal. (par défaut, localhost:3000)
## Liste des routes de l'API

| Route | Verbe HTML     | Description                |
| :-------- | :------- | :------------------------- |
| /api/pictures/ |  GET | Récupère la liste complète des URL des images  |
| /api/pictures/{id} |  GET | Récupère l'URL d'une image d'id donnéen dans le '{id}' |
| /api/pictures/add |  POST | Créer une image avec l'url passé en paramètre (En JSON) |
| /api/pictures/{id} |  UPDATE | Modifie l'image d'id passé dans '{id}' avec les données de l'image passé en JSON|
| /api/pictures/{id} |  DELETE | Supprime l'image d'id passé dans '{id}' |

## Auteur

- [Antoine Pinagot](https://codefirst.iut.uca.fr/git/antoine.pinagot)