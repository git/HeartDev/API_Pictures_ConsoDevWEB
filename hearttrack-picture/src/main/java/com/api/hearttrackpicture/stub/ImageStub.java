package com.api.hearttrackpicture.stub;

import com.api.hearttrackpicture.models.Image;
import com.api.hearttrackpicture.repositories.ImageRepository;

import java.util.ArrayList;
import java.util.List;

public class ImageStub {
    private ImageRepository repository;
    public ImageStub(ImageRepository repo){
        this.repository = repo;
    }

    public void createAndSaveImages(){
        List<Image> l = new ArrayList<>();

        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));
        l.add(new Image(""));

        this.repository.saveAll(l);
    }
}
