package com.api.hearttrackpicture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HearttrackPictureApplication {

	public static void main(String[] args) {
		SpringApplication.run(HearttrackPictureApplication.class, args);
	}

}
