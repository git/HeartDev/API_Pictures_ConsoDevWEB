package com.api.hearttrackpicture.controllers;

import com.api.hearttrackpicture.models.Image;
import com.api.hearttrackpicture.repositories.ImageRepository;
import com.api.hearttrackpicture.stub.ImageStub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class ImageController {
    private final ImageRepository repository;

    // Créer le lien avec le CRUDRepository + initialisation des données grace au Stub
    @Autowired
    public ImageController(ImageRepository repo){
        this.repository = repo;
        new ImageStub(repo).createAndSaveImages();
    }

    // GET - Toutes les URl d'image disponibles
    @GetMapping("/api/pictures/")
    public List<Image> pictures(){
        return (List<Image>) repository.findAll();
    }

    // GET - Affiche une URl avec son id en paramètre
    @GetMapping("/api/pictures/{id}")
    public Optional<Image> pictureById(@PathVariable Long id){
        return repository.findById(id);
    }

    // POST - Ajouter une URL au repository
    @PostMapping(value = "/api/pictures/add",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})

    public ResponseEntity<Image> addImage(@RequestBody Image image) {
        Image persistentImage = repository.save(image);
        return ResponseEntity
                .created(URI
                        .create(String.format("/api/pictures/%s", image.getId())))
                .body(persistentImage);
    }

    // UPDATE - Modifie l'URL grace à son id
    @PutMapping("/api/pictures/{id}")
    public Image replaceUrl(@RequestBody Image newImage, @PathVariable Long id) {
        return repository.findById(id)
                .map(image -> {
                    image.setUrl(newImage.getUrl());
                    return repository.save(image);
                })
                .orElseGet(() -> {
                    newImage.setId(id);
                    return repository.save(newImage);
                });
    }

    // DELETE - Supprime une URL grace à son id
    @DeleteMapping("/api/pictures/{id}")
    void deleteImage(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
