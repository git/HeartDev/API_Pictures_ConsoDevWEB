package com.api.hearttrackpicture.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Image {
    // Id de la classe avec un générateur automatique à chaque création d'objet Image
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String url;

    // Constructeur par défaut
    public Image() {}

    // Constructeur pour le STUB
    public Image(String url){
        this.url = url;
    }

    // Getter/Setter pour l'id
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    // Getter/Setter pour l'url
    public String getUrl(){
        return this.url;
    }

    public void setUrl(String url){
        this.url = url;
    }
}
