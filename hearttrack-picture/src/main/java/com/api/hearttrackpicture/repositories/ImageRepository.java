package com.api.hearttrackpicture.repositories;

import com.api.hearttrackpicture.models.Image;
import org.springframework.data.repository.CrudRepository;

public interface ImageRepository extends CrudRepository<Image, Long> {
}
